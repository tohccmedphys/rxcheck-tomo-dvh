import os
import sys

try:
    from django.conf import settings
    settings.DEBUG
except:
    settings = {}

base = os.path.dirname(__file__)
tests = os.path.join(base, "..", "tests")
test_dvhs = os.path.join(tests, "dvhs")

TOMO_DVH_DIRECTORY = getattr(settings, "TOMO_DVH_DIRECTORY", test_dvhs)
TOMO_PATH_MATCH_RE = "^(?P<patient_id>[0-9a-zA-Z]+)_(?P<plan_id>[~0-9a-zA-Z_]+).*"

GRAY = "Gy"
CENTIGRAY = "cGy"

