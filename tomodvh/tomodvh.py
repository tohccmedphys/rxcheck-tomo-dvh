
from rxcheck_base_importer import BaseImporter

import dvh
import pandas
import numpy as np
import os
import sys

from . import settings


def absolute_file_paths(directory):
    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            yield os.path.abspath(os.path.join(dirpath, f))
        break


def rel_file_paths(directory):
    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            yield f
        break


class TomoDVHImporter(BaseImporter):

    NAME = 'Tomo DVH'
    HANDLE = 'tomodvh'
    MAX_AT_ZERO_VOL = True

    def __init__(self, *args, **kwargs):

        self.import_dir = kwargs.pop('dvh_directory', settings.TOMO_DVH_DIRECTORY)

        super().__init__(*args, **kwargs)

    def get_patients(self):

        paths = rel_file_paths(self.import_dir)

        patients = {}
        for path in paths:

            error = ''
            patient_id = None
            patient_name = None
            verbose_name = None
            plan = {}

            try:
                if not path.endswith('.csv'):
                    continue

                try:
                    dvh_filename = os.path.basename(path)
                except Exception as e:
                    dvh_filename = 'dvh loading error'
                    error = '%s on (%s) message: %s' % (type(e), sys.exc_info()[-1].tb_lineno, e)

                try:
                    csv_reader = pandas.read_csv(os.path.join(self.import_dir, path), delimiter=',', nrows=2, dtype=str)
                    patient_name = csv_reader['txtPatientName'][0]
                    patient_id = csv_reader['txtMedicalID'][0]
                    plan_id = csv_reader['txtPlanName21'][0]
                    del csv_reader

                except Exception as e:
                    plan_id = 'csv_reader error'
                    error = '%s on (%s) message: %s' % (type(e), sys.exc_info()[-1].tb_lineno, e)

                verbose_name = '{}__({})'.format(plan_id, dvh_filename)
                plan = {
                    'name': plan_id,
                    'dvh_filename': dvh_filename,
                    'plan_id': plan_id,
                    'verbose_name': verbose_name,
                    'multiple_with_plan_id': False,
                    'error': error,
                    'patient_name': patient_name
                }

                if patient_id in patients:

                    found_plan_verbose = False
                    for vp in patients[patient_id]:
                        if patients[patient_id][vp]['name'] == plan_id:
                            found_plan_verbose = True
                            patients[patient_id][vp]['multiple_with_plan_id'] = True

                    if found_plan_verbose:
                        plan['multiple_with_plan_id'] = True

                    patients[patient_id][verbose_name] = plan
                else:
                    patients[patient_id] = {
                        verbose_name: plan,
                    }

            except Exception as e:

                err = '%s on (%s) message: %s' % (type(e), sys.exc_info()[-1].tb_lineno, e)
                plan['error'] = err

                if patient_id in patients:
                    patients[patient_id][verbose_name] = plan
                else:
                    patients[patient_id] = {verbose_name: plan}

        return patients

    def get_patient_plan(self, patient_id, plan_id, dvh_filename):
        path = self.find_dvh_path(patient_id, plan_id, dvh_filename)
        if path:
            return self.get_plan_data(path)

    def clean_patient_id(self, patient_id):
        return patient_id

    def find_dvh_path(self, patient_id, plan_id, dvh_filename):
        """return path of the DVH file corresponding to the input patient_id & plan_id.
        """
        csv_reader = pandas.read_csv(os.path.join(self.import_dir, dvh_filename), delimiter=',', nrows=2, dtype=str)
        if patient_id == csv_reader['txtMedicalID'][0] and plan_id == csv_reader['txtPlanName21'][0]:
            return self.import_dir + '\\' + dvh_filename
        del csv_reader
        raise ValueError('Unable to find valid DVH {0} for plan {1} for patient {2}'.format(dvh_filename, plan_id, patient_id))

    def get_plan_data(self, path):
        """return dvh data for input file path."""

        csv_reader = pandas.read_csv(path, delimiter=',', nrows=2, dtype=str)
        patient_name = csv_reader['txtPatientName'][0]
        patient_id = csv_reader['txtMedicalID'][0]
        plan_id = csv_reader['txtPlanName21'][0]
        rx_dose = csv_reader['ValueMaximumDose'][0]
        patient_first_name = patient_name.split(',')[1].strip()
        patient_last_name = patient_name.split(',')[0].strip()

        dvh_filename = os.path.basename(path)

        multiplier = 1.
        csv_reader = pandas.read_csv(path, delimiter=',', usecols=['txtReportTitle'], dtype=str)
        structures_idx = csv_reader.loc[csv_reader['txtReportTitle'] == 'VOI_NAME'].index.item()
        del csv_reader

        csv_reader = pandas.read_csv(
            path,
            header=structures_idx + 1,
            delimiter=',',
            dtype={'VOI_NAME': str, 'Textbox24': str, 'Textbox4': int, 'Textbox25': str, 'Textbox9': float}
        )

        plan = {
            'patient_id': patient_id,
            'name': plan_id,
            'id': plan_id,
            'structures': self.structures_from_data_frame(csv_reader, multiplier, rx_dose),
            'dvh_filename': dvh_filename,
            'patient_first_name': patient_first_name,
            'patient_last_name': patient_last_name
        }
        del csv_reader
        return plan

    def structures_from_data_frame(self, data_frame, multiplier, rx_dose):

        structures = []
        data = {}
        for index, row in data_frame.iterrows():
            voi = row['VOI_NAME']
            vol = float(row['Textbox9'])
            dose = int(row['Textbox4'])

            if voi not in data:
                data[voi] = {
                    'dvh': {
                        'doses': [],
                        'volumes': []
                    }
                }
            data[voi]['dvh']['doses'].append(dose)
            data[voi]['dvh']['volumes'].append(vol)

        for voi in data:
            total_vol = data[voi]['dvh']['volumes'][0]

            orig_arr = np.array(data[voi]['dvh']['volumes'])
            vols = np.array([data[voi]['dvh']['volumes'][index] for index in sorted(np.unique(data[voi]['dvh']['volumes'], return_index=True)[1])])
            doses = np.array([np.average(np.take(data[voi]['dvh']['doses'], np.where(orig_arr == v)[0])) for v in vols])

            for i in range(len(vols)):
                vols[i] = vols[i] / total_vol

            struct_dvh = dvh.DVH(doses, vols, max_at_zero_vol=True)

            structures.append({
                'name': voi.replace("'", ""),
                'volume': total_vol,
                'dvh': {
                    'doses': struct_dvh.doses.tolist(),
                    'volumes': struct_dvh.cum_volumes.tolist(),
                }
            })

        return structures
