#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import glob


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

try:
    readme = open('README.rst').read()
except:
    readme = ''

try:
    history = open('HISTORY.rst').read().replace('.. :changelog:', '')
except:
    history = ''

setup(
    name='rxcheck-tomo-dvh',
    version='3.5.0',
    description='An RxCheck backend for importing DVH data exported from the Tomotherapy treatment planning system',
    long_description=readme + '\n\n' + history,
    author='Ryan Bottema',
    author_email='ryanbottema@gmail.com',
    url='https://bitbucket.org/tohccmedphys'
        '/rxcheck-tomo-dvh',
    packages=[
        'tomodvh',
    ],
    package_dir={'tomodvh': 'tomodvh'},
    data_files=[("tests/dvhs",  glob.glob('tests/dvhs/*'))],
    include_package_data=True,
    install_requires=[
        'dvh',
        'rxcheck-base-importer',
    ],
    license="BSD",
    zip_safe=False,
    keywords='rxcheck-tomo-dvh',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.10',
    ],
    test_suite='tests',
)
