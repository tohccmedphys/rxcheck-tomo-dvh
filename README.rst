===============================
RxCheck Tomo DVH Backend
===============================

.. image:: https://badge.fury.io/py/rxcheck-tomo-dvh.png
    :target: http://badge.fury.io/py/rxcheck-tomo-dvh

.. image:: https://travis-ci.org/tohccmedphys/rxcheck-tomo-dvh.png?branch=master
        :target: https://travis-ci.org/tohccmedphys/rxcheck-tomo-dvh

.. image:: https://pypip.in/d/rxcheck-tomo-dvh/badge.png
        :target: https://pypi.python.org/pypi/rxcheck-tomo-dvh


An RxCheck backend for importing DVH data exported from the
Tomotherapy planning system.

* Free software: BSD license
* Documentation: http://rxcheck-tomo-dvh.rtfd.org.

Features
--------

* TODO
